var assert = require('chai').assert;

module.exports = {
	'requesting a group rate quote from the hood river tour page should populate the referrer field': function (browser) {
		var referrer = 'http://localhost:3000/tours/hood-river';
		
		browser
			.url(referrer)
			.waitForElementVisible('body', 1000, false)
			.click('.requestGroupRate')
			.waitForElementPresent('input[name=referrer]', 1000, false)
			.getValue('input[name=referrer]', function (r) {
				assert.equal(r.value, referrer);
			})
	},
	
	'requesting a group rate from the oregon coast tour page should populate the referrer field': function (browser) {
		var referrer = 'http://localhost:3000/tours/oregon-coast';
		
		browser
			.url(referrer)
			.waitForElementVisible('body', 1000, false)
			.click('.requestGroupRate')
			.waitForElementPresent('input[name=referrer]', 1000, false)
			.getValue('input[name=referrer]', function (r) {
				assert.equal(r.value, referrer);
			})
	},
	
	'visiting the "request group rate" page dirctly should result in an empty referrer field': function (browser) {
		browser
			.url('http://localhost:3000/tours/request-group-rate')
			.waitForElementVisible('body', 1000, false)
			.click('.requestGroupRate', function () {
				browser.getValue('input[name=referrer]', function (r) {
					assert.equal(r.value, '');
				});
			})
			.end();
	}			
};