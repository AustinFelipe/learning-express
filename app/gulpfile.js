var gulp = require('gulp');
var shell = require('gulp-shell');
var jshint = require('gulp-jshint');
var nightwatch = require('gulp-nightwatch');

gulp.task('nightwatch', function () {
	return gulp.src('')
		.pipe(nightwatch({
			configFile: 'nightwatch.json',
			cliArgs: ['--env default']
		}))
		.pipe(nightwatch({
			configFile: 'nightwatch.json',
			cliArgs: ['--env unit']
		}));
});

gulp.task('jshint', function () {
	var toCheck = [
		'gulpfile.js',
		'qa/**/*.js',
		'startup.js',
		'public/qa/**/*.js',
		'public/js/**/*.js',
		'lib/**/*.js'
	];

	return gulp.src(toCheck)
		.pipe(jshint())
		.pipe(jshint.reporter('default'));
});

gulp.task('exec', function () {
	return gulp.src('*.js', { read: false })
		.pipe(shell([
			'linkchecker http://localhost:3000'
		]));
});

gulp.task('default', ['nightwatch', 'jshint', 'exec']);