var fortune = require('../../lib/fortune.js');
var expect = require('chai').expect;

module.exports = {
	'getFortune() should return a fortune': function () {
		expect(typeof fortune.getFortune() === 'string');
	}
};